chip8 : main.o graphics.o chip.o
	gcc -Wall -Werror -Wno-error=unused-but-set-variable main.o graphics.o chip.o -o chip8 $(shell pkg-config --libs sdl)

main.o : main.c 
	gcc -Wall -Werror -Wno-error=unused-but-set-variable -c main.c -o main.o $(shell pkg-config --cflags sdl)

graphics.o : graphics.c
	gcc -Wall -Werror -Wno-error=unused-but-set-variable -c graphics.c -o graphics.o $(shell pkg-config --cflags sdl)

chip.o : chip.c
	gcc -Wall -Werror -Wno-error=unused-but-set-variable -c chip.c -o chip.o
