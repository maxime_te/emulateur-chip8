#include <string.h>
#include <stdio.h>
#include <stdint.h>

#include "headers.h"

// Function to initialize the cpu
void init_cpu(Cpu *cpu)
{
	memset(cpu->memory, 0, MEM_SIZE);
	memset(cpu->V, 0, 16);
	memset(cpu->stack, 0, 16);

	cpu->pc = ROM_START;
	cpu->stack_ptr = 0;
	cpu->cp_game = 0;
	cpu->cp_sound = 0;

	cpu->I = 0;

	memset(cpu->keys, 0, 16);

	int8_t fontset[80] = 
	{
		0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
	    	0x20, 0x60, 0x20, 0x20, 0x70, // 1
	      	0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
	        0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
		0x90, 0x90, 0xF0, 0x10, 0x10, // 4
		0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
		0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
		0xF0, 0x10, 0x20, 0x40, 0x40, // 7
		0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
		0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
		0xF0, 0x90, 0xF0, 0x90, 0x90, // A
		0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
		0xF0, 0x80, 0x80, 0x80, 0xF0, // C
		0xE0, 0x90, 0x90, 0x90, 0xE0, // D
		0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
		0xF0, 0x80, 0xF0, 0x80, 0x80 // F
	};

	int i;
	for(i = 0; i < 80; i++)
	{
		cpu->memory[i] = fontset[i];
	}
}

// Function to decrement game and sound counters
void decr_counters(int *cp_game, int *cp_sound)
{
	if(cp_game > 0)
		cp_game--;
	
	if(cp_sound > 0)
		cp_sound--;
	
	if(cp_sound != 0)
		printf("\a");
}

// Function to load a rom im cpu's memory
int load_rom(char path[], Cpu *cpu)
{
	FILE *rom = NULL;
	rom = fopen(path, "rb");

	if(rom != NULL)
	{
		fread(&cpu->memory[ROM_START], sizeof(int8_t) * (MEM_SIZE - ROM_START), 1, rom);
		fclose(rom);

		return 1;
	}
	else
	{
		fprintf(stderr, "Load ROM error !");
		return 0;
	}
}

// Fonction to read one 16 bits insctructions (two 8 bits instruction)
uint16_t read_instr(Cpu *cpu)
{
	uint16_t instr =  (cpu->memory[cpu->pc] << 8) | cpu->memory[cpu->pc + 1];
	cpu->pc += 2;

	return instr;
}


// The "dispatcher" function
void dispatcher(int16_t instr, Cpu *cpu, Screen *screen, Pixel pixels[][H])
{
	// Extract parameters
	uint16_t _nnn = instr & 0x0FFF;
	uint8_t _nn = instr & 0x00FF;
	uint8_t _n = instr & 0x000F;
	uint8_t _x = (instr & 0x0F00) >> 8;
	uint8_t _y = (instr & 0x00F0) >> 4;
	//printf("_nnn = 0x%x, _nn = 0x%x, _n = 0x%x, _x = 0x%x, _y = 0x%x\n", _nnn, _nn, _n, _x, _y); 

	// Dispatch
	switch(instr & 0xF000)
	{
		case 0x0000:
			switch(_nn)
			{
				case 0xE0: clear_screen(pixels, screen); break; 
				case 0xEE: 
					if(cpu->stack_ptr > 0)
					{
						cpu->stack_ptr -= 1;
						cpu->pc = cpu->stack[cpu->stack_ptr];
					}
				break;
			}
		break;

		case 0x1000: cpu->pc = _nnn; break;
		case 0x2000:
			cpu->stack[cpu->stack_ptr] = cpu->pc;
			cpu->pc = _nnn;
		break;
		case 0x3000:
			if(cpu->V[_x] == _nn)
				cpu->pc += 2;
		break;
		case 0x4000:
			if(cpu->V[_x] != _nn)
				cpu->pc += 2;
		break;
		case 0x5000:
			if(cpu->V[_x] == cpu->V[_y])
				cpu->pc += 2;
		break;
		case 0x6000: cpu->V[_x] = _nn; break;
		case 0x7000: cpu->V[_x] += _nn; break;

		case 0x8000:
			switch(_n)
			{
				case 0x0: cpu->V[_x] = cpu->V[_y]; break;
				case 0x1: cpu->V[_x] = cpu->V[_x] | cpu->V[_y]; break;
				case 0x2: cpu->V[_x] = cpu->V[_x] & cpu->V[_y]; break;
				case 0x3: cpu->V[_x] = cpu->V[_x] ^ cpu->V[_y]; break;
				case 0x4:
					if(cpu->V[_x] + cpu->V[_y] > 255)
						cpu->V[0xF] = 1;
					else
						cpu->V[0xF] = 0;
					
					cpu->V[_x] += cpu->V[_y];
				break;
				case 0x5:
					if(cpu->V[_y] > cpu->V[_x])
						cpu->V[0xF] = 1;
					else
						cpu->V[0xF] = 0;

					cpu->V[_y] -= cpu->V[_x];
				break;
				case 0x6:
					cpu->V[0xF] = cpu->V[_x] & 0x01;
					cpu->V[_x] >>= 1;
				break;
				case 0x7:
					if(cpu->V[_x] > cpu->V[_y])
						cpu->V[0xF] = 1;
					else
						cpu->V[0xF] = 0;

					cpu->V[_x] -= cpu->V[_y];
				break;
				case 0xE:
					cpu->V[0xF] = (cpu->V[_x] & 0x80) >> 7;
					cpu->V[_x] <<= 1;
				break;
			}
		break;

		case 0x9000: 
			if(cpu->V[_x] != cpu->V[_y])
				cpu->pc += 2;
		break;
		case 0xA000: cpu->I = _nnn; break;
		case 0xB000: cpu->pc = _nnn + cpu->V[0x0]; break;
		case 0xC000: cpu->V[_x] = rand() & _nn; break;
		case 0xD000:
		{
			int x, y, shift;
			int code_line;
			int co_x = 0, co_y = 0;
			cpu->V[0xF] = 0x0;

			for(y = 0; y < _n; y++)
			{
				code_line = cpu->memory[cpu->I + y];
				co_y = (cpu->V[_y] + y) % H;
				
				for(x = 0, shift = 0; x < 8; x++, shift--)
				{
					co_x = (cpu->V[_x] + x) % W;
					if((code_line & (0x80 >> x)) != 0)
					{
						if(pixels[co_x][co_y].color == WHITE)
						{
							pixels[co_x][co_y].color = BLACK;
							cpu->V[0xF] = 0x01;
						}
						else
							pixels[co_x][co_y].color = WHITE;
					}
				}
			}
		break;
		}

		case 0xE000:
			switch(_nn)
			{
				case 0x9E:
					if(cpu->keys[cpu->V[_x]] == 1)
						cpu->pc += 2;
				break;

				case 0xA1:
					if(cpu->keys[cpu->V[_x]] == 0)
						cpu->pc += 2;
				break;
			}
		break;

		case 0xF000:
			switch(_nn)
			{
				case 0x07: cpu->V[_x] = cpu->cp_game; break;
				case 0x0A:
				{
					int i;
					for(i = 0; i < 16; i++)
					{
						if(cpu->keys[i] == 1)
							cpu->V[_x] = i;
					}
				break;
				}
				case 0x15: cpu->cp_game = cpu->V[_x]; break;
				case 0x18: cpu->cp_sound = cpu->V[_x]; break;
				case 0x1E: cpu->I += cpu->V[_x]; break;
				case 0x29: cpu->I = cpu->V[_x] * 5; break;
				case 0x33:
					cpu->memory[cpu->I] = cpu->V[_x] / 100;
					cpu->memory[cpu->I + 1] = (cpu->V[_x] % 100) / 10;
					cpu->memory[cpu->I + 2] = (cpu->V[_x] % 100) % 10;
				break;
				case 0x55:
				{
					int i;
					for(i = 0; i < _x; i++)
						cpu->memory[cpu->I + i] = cpu->V[i];
				break;
				}
				case 0x65:
				{
					int i;
					for(i = 0; i < _x; i++)
						cpu->V[i] = cpu->memory[cpu->I + i];
				break;
				}
			}
		break;
	}
}

