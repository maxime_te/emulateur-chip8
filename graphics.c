#include <SDL/SDL.h>

#include "headers.h"

// SDL Loop
void loop()
{
	int ok = 1;
	
	while(ok)
	{
		SDL_WaitEvent(&event);
		switch(event.type)
		{
			case SDL_QUIT:
				ok = 0;
			break;
		}
	}
}

// Function to listen event during the emulation (to quit)
int listen(Cpu *cpu)
{
	int ok = 1;
	while(SDL_PollEvent(&event))
	{
		switch(event.type)
		{
			case SDL_QUIT: ok = 0; break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym)
				{
					case SDLK_0: cpu->keys[0x0] = 1; break;
					case SDLK_1: cpu->keys[0x1] = 1; break;
					case SDLK_2: cpu->keys[0x2] = 1; break;
					case SDLK_3: cpu->keys[0x3] = 1; break;
					case SDLK_4: cpu->keys[0x4] = 1; break;
					case SDLK_5: cpu->keys[0x5] = 1; break;
					case SDLK_6: cpu->keys[0x6] = 1; break;
					case SDLK_7: cpu->keys[0x7] = 1; break;
					case SDLK_8: cpu->keys[0x8] = 1; break;
					case SDLK_9: cpu->keys[0x9] = 1; break;

					case SDLK_a: cpu->keys[0xA] = 1; break;
					case SDLK_z: cpu->keys[0xB] = 1; break;
					case SDLK_e: cpu->keys[0xC] = 1; break;
					case SDLK_r: cpu->keys[0xD] = 1; break;
					case SDLK_t: cpu->keys[0xE] = 1; break;
					case SDLK_y: cpu->keys[0xF] = 1; break;

					default:
					break;
				}
			break;
			case SDL_KEYUP:
				switch(event.key.keysym.sym)
				{
					case SDLK_0: cpu->keys[0x0] = 0; break;
					case SDLK_1: cpu->keys[0x1] = 0; break;
					case SDLK_2: cpu->keys[0x2] = 0; break;
					case SDLK_3: cpu->keys[0x3] = 0; break;
					case SDLK_4: cpu->keys[0x4] = 0; break;
					case SDLK_5: cpu->keys[0x5] = 0; break;
					case SDLK_6: cpu->keys[0x6] = 0; break;
					case SDLK_7: cpu->keys[0x7] = 0; break;
					case SDLK_8: cpu->keys[0x8] = 0; break;
					case SDLK_9: cpu->keys[0x9] = 0; break;

					case SDLK_a: cpu->keys[0xA] = 0; break;
					case SDLK_z: cpu->keys[0xB] = 0; break;
					case SDLK_e: cpu->keys[0xC] = 0; break;
					case SDLK_r: cpu->keys[0xD] = 0; break;
					case SDLK_t: cpu->keys[0xE] = 0; break;
					case SDLK_y: cpu->keys[0xF] = 0; break;

					default:
					break;
				}
		}
	}

	return ok;
}

// Function to initialize pixels tab
void init_pixels(Pixel pixels[][H])
{
	int x = 0, y = 0;
	for(x = 0; x < W; x++)
	{
		for(y = 0; y < H; y++)
		{
			pixels[x][y].position.x = x * PIXEL;
			pixels[x][y].position.y = y * PIXEL;
			pixels[x][y].color = BLACK;
			
			/*
			if(x % (y + 1) == 0)
				pixels[x][y].color = WHITE;
			else
				pixels[x][y].color = BLACK;
			*/	
		}
	}
}	

// Function to reset screen
void clear_screen(Pixel pixels[][H], Screen *screen)
{
	int x = 0, y = 0;
	for(x = 0; x < W; x++)
	{
		for(y = 0; y < H; y++)
		{
			pixels[x][y].color = BLACK;
		}
	}

	SDL_FillRect(screen->screen, NULL, SDL_MapRGB(screen->screen->format, 255, 255, 255));
}

// Function to draw (sdl blit) bw_surface on screen
void draw_pixel(Pixel *pixel, Screen *screen)
{
	SDL_Rect pos_pixel = pixel->position;
	SDL_BlitSurface(screen->bw_surfaces[pixel->color], NULL, screen->screen, &pos_pixel);
}

// Function to draw all the pixels
void draw_all_pixels(Pixel pixels[][H], Screen *screen)
{
	int x = 0, y = 0;
	for(x = 0; x < W; x++)
	{
		for(y = 0; y < H; y++)
		{
			draw_pixel(&pixels[x][y], screen);
		}
	}

	SDL_Flip(screen->screen);
}

