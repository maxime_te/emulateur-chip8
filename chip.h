#ifndef CHIP
#define CHIP

#include <stdint.h>
#include "graphics.h"

#define MEM_SIZE 4096
#define ROM_START 0x200

#define CLOCK 4		// Operation number per tour
#define FPS 16		


// Struct to implement the CPU
typedef struct Cpu Cpu;
struct Cpu
{
	int8_t memory[MEM_SIZE];
	int16_t pc;
	int8_t V[16];
	int16_t I;

	int8_t stack[16];
	int stack_ptr;

	int cp_game;
	int cp_sound;

	// Keyboard
	int keys[16];
};

// Prototypes
void init_cpu(Cpu *cpu);
void decr_counters(int *cp_game, int *cp_sound);
int load_rom(char path[], Cpu *cpu);
uint16_t read_instr(Cpu *cpu);
void dispatcher(int16_t instr, Cpu *cpu, Screen *screen, Pixel pixels[][H]);

#endif
