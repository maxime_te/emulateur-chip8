#include <stdlib.h>
#include <SDL/SDL.h>

#include "headers.h"


int main(int argc, char *argv[])
{
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "%s\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}

	// Init screen struct
	Screen screen;
	
	screen.screen = NULL;
	screen.screen = SDL_SetVideoMode(WIDTH, HEIGH, 32, SDL_HWSURFACE);
	if(screen.screen == NULL)
	{
		fprintf(stderr, "%s\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}

	// Pixels tab (64*32)
	Pixel pixels[W][H];
	init_pixels(pixels);	

	// Create Black and White SDL_Surface (Screen struct)
	screen.bw_surfaces[0] = SDL_CreateRGBSurface(SDL_HWSURFACE, PIXEL, PIXEL, 32, 0, 0, 0, 0);
	screen.bw_surfaces[1] = SDL_CreateRGBSurface(SDL_HWSURFACE, PIXEL, PIXEL, 32, 0, 0, 0, 0);
	if(screen.bw_surfaces[0] == NULL || screen.bw_surfaces[1] == NULL)
	{
		fprintf(stderr, "%s\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}

	SDL_FillRect(screen.bw_surfaces[0], NULL, SDL_MapRGB(screen.bw_surfaces[0]->format, 0, 0, 0));

	SDL_FillRect(screen.bw_surfaces[1], NULL, SDL_MapRGB(screen.bw_surfaces[1]->format, 255, 255, 255));

	draw_all_pixels(pixels, &screen);

	// Execute operation
	Cpu cpu;
	init_cpu(&cpu);

	if(argc > 1)
	{
		if(load_rom(argv[1], &cpu) == 1)
			printf("ROM loaded !\n");
	}

	int go, cp_clock = 0;
	do
	{
		for(cp_clock = 0; cp_clock < CLOCK; cp_clock++)
		{
			// Execute
			dispatcher(read_instr(&cpu), &cpu, &screen, pixels);
		}

		// Refresh
		draw_all_pixels(pixels, &screen);
		SDL_Delay(FPS);
		decr_counters(&cpu.cp_game, &cpu.cp_sound);
	
	 	// SDL Main event loop
		go = listen(&cpu);
	}while(go);

	// SDL Main event loop
	loop();	

	// Quit
	SDL_Quit();
	return EXIT_SUCCESS;
}

