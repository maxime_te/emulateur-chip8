#ifndef GRAPHICS
#define GRAPHICS

#include <SDL/SDL.h>

#define W 64
#define H 32

#define PIXEL 8

#define WIDTH W * PIXEL
#define HEIGH H * PIXEL

#define BLACK 0
#define WHITE 1

#include <stdint.h>

#define MEM_SIZE 4096
#define ROM_START 0x200

#define CLOCK 4		// Operation number per tour
#define FPS 16		


// Pixel struct

typedef struct Pixel Pixel;
struct Pixel
{
	SDL_Rect position;
	int color;
};

// Screen struct

typedef struct Screen Screen;
struct Screen
{
	SDL_Surface *screen, *bw_surfaces[2];
};

// SDL Event

SDL_Event event;

// Struct to implement the CPU
typedef struct Cpu Cpu;
struct Cpu
{
	int8_t memory[MEM_SIZE];
	int16_t pc;
	int8_t V[16];
	int16_t I;

	int8_t stack[16];
	int stack_ptr;

	int cp_game;
	int cp_sound;

	// Keyboard
	int keys[16];
};

// Prototypes
void init_cpu(Cpu *cpu);
void decr_counters(int *cp_game, int *cp_sound);
int load_rom(char path[], Cpu *cpu);
uint16_t read_instr(Cpu *cpu);
void dispatcher(int16_t instr, Cpu *cpu, Screen *screen, Pixel pixels[][H]);



// Prototypes
void loop();
int listen(Cpu *cpu);
void init_pixels(Pixel pixels[][H]);
void clear_screen(Pixel pixels[][H], Screen *screen);
void draw_pixel(Pixel *pixel, Screen *screen);
void draw_all_pixels(Pixel pixels[][H], Screen *screen);


#endif
